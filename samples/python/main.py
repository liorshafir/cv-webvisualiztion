import asyncio
import websockets
import flatbuffers
from flatbuffers import builder

# Generated by `flatc`.
import webviewer.Box
import webviewer.Color
import webviewer.Vec3
import webviewer.Image
import webviewer.Line
import webviewer.Frame
import numpy as np

async def send_frame(websocket, path):
    # Create a `FlatBufferBuilder`, which will be used to create our
    # frame FlatBuffers.
    fbuilder = flatbuffers.builder.Builder(1024)
    # Create some Point

    box_name = fbuilder.CreateString("Sample-Box")
    box_type = fbuilder.CreateString("Car")

    webviewer.Box.BoxStart(fbuilder)
    webviewer.Box.BoxAddName(fbuilder, box_name)
    webviewer.Box.BoxAddType(fbuilder, box_type)
    webviewer.Box.BoxAddScore(fbuilder, 99.0)
    box_rotation = webviewer.Vec3.CreateVec3(fbuilder, 0, 0, 0)
    webviewer.Box.BoxAddRotation(fbuilder, box_rotation)
    box_pos = webviewer.Vec3.CreateVec3(fbuilder, 10, 0, 0)
    webviewer.Box.BoxAddPos(fbuilder, box_pos)
    box_size = webviewer.Vec3.CreateVec3(fbuilder, 10, 10, 10)
    webviewer.Box.BoxAddSize(fbuilder, box_size)
    box = webviewer.Box.BoxEnd(fbuilder)
    webviewer.Frame.FrameStartBoxesVector(fbuilder, 1)
    fbuilder.PrependSOffsetTRelative(box)
    boxes = fbuilder.EndVector(1)

    # x1, y1, z1, x2, y2, z2, ..
    points_arr = np.array([1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0], dtype=np.float32)
    # r1, r2, ..
    reflectivities_arr = np.array([120, 120, 120], dtype=np.ubyte)

    points = fbuilder.CreateNumpyVector(points_arr)
    reflectivities = fbuilder.CreateNumpyVector(reflectivities_arr)

    webviewer.Frame.FrameStart(fbuilder)
    webviewer.Frame.FrameAddId(fbuilder, 1)
    webviewer.Frame.FrameAddBoxes(fbuilder, boxes)
    webviewer.Frame.FrameAddPoints(fbuilder, points)
    webviewer.Frame.FrameAddReflectivities(fbuilder, reflectivities)
    frame = webviewer.Frame.FrameEnd(fbuilder)
    fbuilder.Finish(frame)
    buf = fbuilder.Output()
    await websocket.send(buf)

start_server = websockets.serve(send_frame, "localhost", 8080)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()