// cv-web.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "viewerschemav4_generated.h"
#include <boost/lambda/lambda.hpp>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <cstdlib>
#include <cstdlib>
#include <functional>
#include <thread>
#include <memory>
#include <utility>
#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast.hpp>

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

//using boost::asio::ip::tcp;
using namespace Cv::WebViewer;

class session
	: public std::enable_shared_from_this<session>
{
public:
	session(tcp::socket socket)
		: socket_(std::move(socket))
	{
	}

	void start()
	{
		//auto self(shared_from_this());
		//flatbuffers::FlatBufferBuilder builder(1024);
		//auto frame_name = builder.CreateString("SampleFrame");

		//float points[] = { 1.0, 1.0, 1.0, 1.0, 0, 0};
		//auto frame_points = builder.CreateVector(points, 6);
		//unsigned char reflectivities[] = { 120, 120 };
		//auto frame_reflectivities = builder.CreateVector(reflectivities, 2);

		//FrameBuilder frame_builder(builder);
		//frame_builder.add_name(frame_name);
		//frame_builder.add_points(frame_points);
		//frame_builder.add_reflectivities(frame_reflectivities);
		//auto frame = frame_builder.Finish();
		//builder.Finish(frame);
		//uint8_t* buf = builder.GetBufferPointer();
		//int size = builder.GetSize(); // Returns the size of the buffer that

		//
		//boost::asio::async_write(socket_, boost::asio::buffer(buf, size),
		//	[this, self](boost::system::error_code ec, std::size_t /*length*/)
		//	{
		//		if (!ec)
		//		{
		//			do_read();
		//		}
		//	});
		do_read();
	}

private:
	void do_read()
	{
		auto self(shared_from_this());
		socket_.async_read_some(boost::asio::buffer(data_, max_length),
			[this, self](boost::system::error_code ec, std::size_t length)
			{
				if (!ec)
				{
					do_write(length);
				}
			});
	}

	void do_write(std::size_t length)
	{
		auto self(shared_from_this());
		boost::asio::async_write(socket_, boost::asio::buffer(data_, length),
			[this, self](boost::system::error_code ec, std::size_t /*length*/)
			{
				if (!ec)
				{
					do_read();
				}
			});
	}

	tcp::socket socket_;
	enum { max_length = 1024 };
	char data_[max_length];
};

class server
{
public:
	server(boost::asio::io_service& io_service, short port)
		: acceptor_(io_service, tcp::endpoint(tcp::v4(), port)),
		socket_(io_service)
	{
		do_accept();
	}

private:
	void do_accept()
	{
		acceptor_.async_accept(socket_,
			[this](boost::system::error_code ec)
			{
				if (!ec)
				{
					std::make_shared<session>(std::move(socket_))->start();
				}

				do_accept();
			});
	}

	tcp::acceptor acceptor_;
	tcp::socket socket_;
};


void send_frame(websocket::stream<tcp::socket>& ws, int num_points, float pts[], unsigned char reflectivities[]) {
	// This buffer will hold the incoming message
	beast::flat_buffer buffer;

	ws.binary(true);
	flatbuffers::FlatBufferBuilder builder(1024);

	auto box_name = builder.CreateString("Test Box");
	auto box_type = builder.CreateString("Test");
	auto box_comment = builder.CreateString("Comment");
	float score = 0.6;

	auto box_position = Vec3(10.0f, 2.0f, 3.0f);
	auto box_size = Vec3(10.0f, 10.0f, 10.0f);
	auto box_rotation = Vec3(0.0f, 0.0f, 0.0f);
	auto box = CreateBox(builder, box_name, box_type, &box_position, &box_size, &box_rotation, NULL, score, box_comment);
	std::vector<flatbuffers::Offset<Box>> boxes_vector;
	boxes_vector.push_back(box);
	auto boxes = builder.CreateVector(boxes_vector);

	auto frame_name = builder.CreateString("SampleFrame");
	auto frame_points = builder.CreateVector(pts, num_points);
	auto frame_reflectivities = builder.CreateVector(reflectivities, 2);

	FrameBuilder frame_builder(builder);
	frame_builder.add_name(frame_name);
	frame_builder.add_points(frame_points);
	frame_builder.add_reflectivities(frame_reflectivities);
	frame_builder.add_boxes(boxes);
	auto frame = frame_builder.Finish();
	builder.Finish(frame);
	uint8_t* buf = builder.GetBufferPointer();
	int size = builder.GetSize(); // Returns the size of the buffer that

	ws.write(boost::asio::buffer(buf, size));
}

void do_session(tcp::socket& socket)
{
	try
	{
		// Construct the stream by moving in the socket
		websocket::stream<tcp::socket> ws{ std::move(socket) };

		ws.binary(true);
		// Set a decorator to change the Server of the handshake
		ws.set_option(websocket::stream_base::decorator(
			[](websocket::response_type& res)
			{
				res.set(http::field::server,
					std::string(BOOST_BEAST_VERSION_STRING) +
					" websocket-server-sync");
			}));

		// Accept the websocket handshake
		ws.accept();

		unsigned char reflectivities[] = { 120, 120 };
		float points[] = { 1.0, 1.0, 1.0, 1.0, 0, 0 };
		send_frame(ws, 6, points, reflectivities);

	}
	catch (beast::system_error const& se)
	{
		// This indicates that the session was closed
		if (se.code() != websocket::error::closed)
			std::cerr << "Error: " << se.code().message() << std::endl;
	}
	catch (std::exception const& e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
	}
}




int main()
{
	auto const address = net::ip::make_address("127.0.0.1");
	auto const port = static_cast<unsigned short>(8080);

	// The io_context is required for all I/O
	net::io_context ioc{ 1 };

	// The acceptor receives incoming connections
	tcp::acceptor acceptor{ ioc, {address, port} };
	for (;;)
	{
		// This will receive the new connection
		tcp::socket socket{ ioc };

		// Block until we get a connection
		acceptor.accept(socket);

		// Launch the session, transferring ownership of the socket
		std::thread{ std::bind(
			&do_session,
			std::move(socket)) }.detach();
	}
	
    return 0;
}

