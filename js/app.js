function App() {	
    this.frames = [];
	//this.frames = {};
	this.current_frame;
	this.current_pc;
    this.network = null;
    this.numViews = 1;
    

    this.init = function() {
        network = new NetworkClient();
        if (this.socket.areadyState != WebSocket.CLOSED)
        {
            $("#btnNetwork").text("Disconnect");
        }

    }

    this.singleView = function () {
        $(".row2").hide()
        $(".row1").removeClass('h-50');
        $(".row1").addClass('h-100');
        $("#viewer1").removeClass('col-sm-6');
        $("#viewer1").addClass('col-sm-12');
        $("#viewer2").hide();
        numViews = 1;
        onWindowResize()
    };  

    this.doubleView = function () {
        $(".row2").hide()
        $(".row1").removeClass('h-50');
        $(".row1").addClass('h-100');
        $("#viewer2").show();
        $("#viewer1").removeClass('col-sm-12');
        $("#viewer1").addClass('col-sm-6');
        numViews = 2;
        onWindowResize()
    };

    this.FourView = function () {
        $(".row2").show()
        $(".row1").removeClass('h-100')
        $(".row1").addClass('h-50')
        $("#viewer2").show();
        $("#viewer1").removeClass('col-sm-12');
        $("#viewer1").addClass('col-sm-6');
        numViews = 4;
        onWindowResize()
    };

    $("#frames-range-slider").on("input change", function(e) {
        app.ShowFrame(app.frames[$("#frames-range-slider").val()]);
    });

    $('.btn-expand-collapse').click(function(e) {
        $('.navbar-primary').toggleClass('collapsed');
        if ($('.navbar-primary').hasClass('collapsed'))
            $('.main-container').css('margin-left', '60px');
        else
            $('.main-container').css('margin-left', '200px');
    });

    $("#btn1").click(function() {
        app.singleView();
    });

    $("#btn2").click(function() {
        app.doubleView();
    });

    $("#btn4").click(function() {
        app.FourView();
    });

    $("#btnNetwork").click(function() {
        if (app.socket.readyState === WebSocket.CLOSED) {
            this.network = new NetworkClient();
            $("#btnNetwork").text("Disconnect");
        }
        else {
            app.socket.close();
            $("#btnNetwork").text("Connect");
        }
    });

    $("#btnClearFrames").click(function() {
        app.ClearFrames();
    })

    this.ClearFrames = function() {
        var ul = document.getElementById("navbar-primary-frame-list");
        $(ul).empty()
        app.frames = [];   
    }

    this.AddFrame = function(newFrame) {
        this.AddToFrameList(newFrame)
        this.ShowFrame(newFrame);
    }

    this.ShowFrame = function(newFrame) {
        var vertices = [];
        var numPoints = newFrame.pointsLength()  / 3;
        var numBoxes = newFrame.boxesLength();
            
        var curTime = new Date().getTime();
        var time = curTime - previousTime;
        console.log("new message " + numMessages + ", time: " + time + " ms");
        previousTime = curTime;
        numMessages++;
    
        var isUpdatedPc = updatePointCloud(newFrame.pointsArray(), newFrame.reflectivitiesArray());
        
        var cssboxes = [];
        var geoboxes = [];
        for (var i = 0; i < sceneInfos.length; i++)
        {
            var meshBoxes = [];
            var sceneBoxes = [];
            for (var ind = 0; ind < numBoxes; ind++) {
                var rand = ( Math.random() * 0.5 + 0.25 );
                var element1 = document.createElement( 'div' );
                element1.className = 'element';
                element1.style.backgroundColor = 'rgba(0,127,127,' + rand + ')';
                element1.style.width =  newFrame.boxes(ind).size().y() * 100 + 'px';
                element1.style.height = newFrame.boxes(ind).size().x() * 100 + 'px';            
                var number = document.createElement( 'div' );
                number.className = 'number';
                number.textContent = ( i / 5 ) + 1;
                element1.appendChild( number );
                var symbol = document.createElement( 'div' );
                symbol.className = 'symbol';
                symbol.textContent = newFrame.boxes(ind).type();
                symbol.style.fontSize = parseInt(newFrame.boxes(ind).size().y() * 120 / newFrame.boxes(ind).type().length) + 'px';
                element1.appendChild( symbol );
                var details = document.createElement( 'div' );
                details.className = 'details';
                details.innerHTML = "Easy" + '<br>' + "1.0";
                element1.appendChild( details );
                var object = new THREE.CSS3DObject( element1 );
                var pos = new THREE.Vector3(newFrame.boxes(ind).pos().x() * 100, newFrame.boxes(ind).pos().y() * 100, newFrame.boxes(ind).pos().z() * 100 + newFrame.boxes(ind).size().z() * 100);
                var rotation = new THREE.Euler(0, 0,  - 90 * THREE.Math.DEG2RAD + newFrame.boxes(ind).rotation().z())
                object.position.copy(pos);
                object.rotation.copy(rotation);
                                
                var pos = new THREE.Vector3(newFrame.boxes(ind).pos().x() * 100, newFrame.boxes(ind).pos().y() * 100, newFrame.boxes(ind).pos().z() * 100 + newFrame.boxes(ind).size().z() * 100);
                var rotation = new THREE.Euler(0, 0,  - 90 * THREE.Math.DEG2RAD + newFrame.boxes(ind).rotation().z())
                object.position.copy(pos);
                object.rotation.copy(rotation);
                var geometry = new THREE.BoxBufferGeometry( newFrame.boxes(ind).size().y() * 100 , newFrame.boxes(ind).size().x() * 100, newFrame.boxes(ind).size().z() * 100);
                var edges = new THREE.EdgesGeometry( geometry );
                var line = new THREE.LineSegments( edges, new THREE.LineBasicMaterial( { color: 0x7AFFFF } ) );
                line.position.set(object.position.x, object.position.y, object.position.z - newFrame.boxes(ind).size().z() * 50)
                line.rotation.setFromVector3(object.rotation.toVector3());                
                sceneBoxes.push(object);
                meshBoxes.push(line);
            }
            cssboxes.push(sceneBoxes);
            geoboxes.push(meshBoxes);
        }
        //if (isUpdatedPc == 0)
        addBox(geoboxes, cssboxes);
        if (newFrame.imagesLength() > 0)
        {
            addImage(newFrame.images(0).width(), newFrame.images(0).height(), newFrame.images(0).rawdataArray());
        }
        render();
               
    }

    this.AddToFrameList = function(newFrame) {
        var numFrames = app.frames.length;
        var ul = document.getElementById("navbar-primary-frame-list");
        var li = document.createElement("li");
        
        $("#frames-range-slider")[0].max = numFrames.toString();     
        $("#frames-range-slider").val(numFrames - 1)        
        
        li.appendChild(document.createTextNode(numFrames.toString() + ": " + newFrame.name()));
        ul.appendChild(li);
        app.frames.push(newFrame);
    }
}



