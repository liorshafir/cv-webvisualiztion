var connectionString = 'ws://127.0.0.1:8080';
//var connectionString = 'ws://10.71.81.160:8080';


function NetworkClient() {
    socket = new WebSocket(connectionString);
    socket.binaryType = 'arraybuffer';
    numMessages = 0;
    socket.addEventListener('open', function (event) {    
    previousTime = new Date().getTime();
    });

    app.socket = socket;
    socket.addEventListener('message', function (event) {
        var bytes = new Uint8Array(event.data);/* the data you just read, in an object of type "Uint8Array" */
        if (bytes.length == 8 && bytes[0] == 255 && bytes[1] == 255  && bytes[2] == 255  && bytes[3] == 255  && bytes[4] == 255  && bytes[5] == 255 && bytes[6] == 255 && bytes[7] == 255)
        {
            app.ClearFrames();
        }
        else 
        {
            var buf = new flatbuffers.ByteBuffer(bytes);
            // Get an accessor to the root object inside the buffer.
            var newFrame = Cv.WebViewer.Frame.getRootAsFrame(buf);
            
            
            app.AddFrame(newFrame);
        }
    });	

}



