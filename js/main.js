THREE.Object3D.DefaultUp.y = 0;
THREE.Object3D.DefaultUp.z = 1;		
var x = THREE.Object3D.DefaultUp;
if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

colormap = init_color_map();			

protobufRoot = null;

var particleLight;
var ray, brush, objectHovered,								
mouse3D, isMouseDown = false, onMouseDownPosition,
radious = 1600, theta = 45, onMouseDownTheta = 45, phi = 60, onMouseDownPhi = 60,
sceneInfos = []
isShiftDown = false;
numViews = 4;
frameSequence = 0;

onMouseDownPosition = new THREE.Vector2();
deltaRotation = new THREE.Vector2();
deltaTranslation = new THREE.Vector2();
var app;
var loader = new THREE.FontLoader();

init();

function makeScene(elem) {
    var scene = new THREE.Scene();
    var cssScene = new THREE.Scene();
    const fov = 40;
    const aspect = 2;  // the canvas default // check why not window.innerWidth / window.innerHeight
    const near = 0.4;
    const far = 400 * 100;
    var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    camera.position.set( -0.8,  -1.2,  5 );
    //futureTarget = new THREE.Vector3( -0.8,  -1.2,  2);   
    camera.rotation.set(+1 * Math.PI /2, 0  , -1 * Math.PI /2, "ZYX")
    var targetRotation =  new THREE.Vector3 ( +1 * Math.PI /2, 0, -1 * Math.PI /2 );
    var originalRotation = new THREE.Vector3 ( +1 * Math.PI /2, 0, -1 * Math.PI /2 );
    var targetTranslation = new THREE.Vector3(-0.8,  -1.2,  5);
    var originalTranslation = new THREE.Vector3( -0.8,  -1.2,  5  );

   


    loader.load( '../three.js-master/examples/fonts/gentilis_regular.typeface.json', function ( font ) {
        initWorld(scene, font);
    });
    var container = document.createElement( 'div' );
    elem.appendChild(container);
    
    var renderer = new THREE.WebGLRenderer( { antialias: true } );
    var cssRenderer = new THREE.CSS3DRenderer({ antialias: true });
    //sceneIn.camera.aspect = rect.width / rect.height;    




    var rect = elem.getBoundingClientRect();
    //renderer.setPixelRatio( rect.width / rect.height );
    renderer.setSize( rect.width, rect.height );
    container.appendChild( renderer.domElement );
    renderer.domElement.style.position = 'absolute';
    renderer.domElement.style.top = 0;

    //cssRenderer.setPixelRatio( rect.width / rect.height );
    cssRenderer.setSize( rect.width, rect.height)
    cssRenderer.domElement.style.position = 'absolute';
    cssRenderer.domElement.style.top = 0;
        
    camera.aspect = rect.width / rect.height;
    camera.updateProjectionMatrix();

    container.appendChild( cssRenderer.domElement );

    var controls = new THREE.OrbitControls( camera, cssRenderer.domElement );
    controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)
    controls.noZoom = false;
    controls.noPan = false;
    
    
    //controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
    //controls.dampingFactor = 0.05;
    //controls.screenSpacePanning = false;
    //controls.minDistance = 100;
    //controls.maxDistance = 50000;
    //controls.maxPolarAngle = Math.PI / 2;

    stats = new Stats();
    elem.appendChild( stats.dom );
    $(stats.dom).css('position', 'absolute');
    renderer.gammaInput = true;
    renderer.gammaOutput = true;

    // {
    //   const color = 0xFFFFFF;
    //   const intensity = 1;
    //   const light = new THREE.DirectionalLight(color, intensity);
    //   light.position.set(-1, 2, 4);
    //   scene.add(light);
    // }
    return {renderer, cssRenderer, scene, cssScene, camera, elem, targetRotation, stats, controls};
}

function registerEvents(elem) {
    elem.addEventListener( 'mousemove', onDocumentMouseMove, false );
    elem.addEventListener( 'mousedown', onDocumentMouseDown, false );
    elem.addEventListener( 'mouseup', onDocumentMouseUp, false );
    elem.addEventListener( 'wheel', onDocumentMouseWheel, );
    elem.addEventListener( 'keydown', onDocumentKeyDown);  
}

function setupScene1() {
    var elem = document.querySelector('#viewer1')

    const sceneInfo = makeScene(elem);
    sceneInfo.camera.position.set(-330.0, 0.0, 4200.0);
    sceneInfo.camera.rotation.set(0, 0, 10000, "ZYX");
    sceneInfo.camera.lookAt(0, 0, 100, "ZYX");
    sceneInfo.controls.update();
    //registerEvents(elem);
    return sceneInfo;
}

function setupScene2() {
    var elem = document.querySelector('#viewer2')
    const sceneInfo = makeScene(elem);
    sceneInfo.camera.position.set(4330, 0.0, 12200.0);
    sceneInfo.camera.rotation.set(0, 0  , -1 * Math.PI /2, "ZYX");
    sceneInfo.controls.update();
    //registerEvents(elem);
    return sceneInfo;
}

function setupScene3() {
    var elem = document.querySelector('#viewer3')
    const sceneInfo = makeScene(elem);
    sceneInfo.camera.position.set(300.0, 900.0, 300.0);
    sceneInfo.camera.rotation.set(Math.PI /2, 0, 1 * Math.PI, "ZYX");
    sceneInfo.controls.update();
    //registerEvents(elem);
    return sceneInfo;
}

function resizeRendererToDisplaySize(renderer) {
    const canvas = renderer.domElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const needResize = canvas.width !== width || canvas.height !== height;
    if (needResize) {
      renderer.setSize(width, height, false);
    }
    return needResize;
}


function init() {
    // var splitter = $('#viewspacewrapper').height(800).split({
    //     orientation: 'vertical',
    //     limit: 10,
    //     position: '50%', 
    // });
        
    const viewSpace = document.querySelector('#viewspacewrapper');
    
    sceneInfo1 = setupScene1();
    sceneInfos.push(sceneInfo1);
    sceneInfo2 = setupScene2();
    sceneInfos.push(sceneInfo2);
    sceneInfo3 = setupScene3();
    sceneInfos.push(sceneInfo3);

    window.addEventListener( 'resize', onWindowResize, false );
    app = new App();
    app.init();
    app.singleView();
    animate();
}


function initWorld(scene, font) {
    
    for (var rad = 0; rad <= 150; rad++) {
        drawRing(scene, rad, 0x7595ff, 0.235);
    }

    for (var rad = 0; rad <= 150; rad+=10) {
        //addLabel( rad.toString(), new THREE.Vector3( rad, 0, 0 ), 0x7595ff, 0.47 ,0.8, font);
        drawRing(scene, rad, 0x7595ff, 0.47);
    }

    for (var rad = 0; rad <= 150; rad+=50) {
        drawRing(scene, rad, 0x7595ff, 0.9);
    }
    
}



function drawRing(scene, rad, ringColor, opacity) {
    var curve = new THREE.EllipseCurve(
            0,  0,            // ax, aY
            rad * 100, rad * 100,           // xRadius, yRadius
            0,  2 * Math.PI,  // aStartAngle, aEndAngle
            false,            // aClockwise
            30                 // aRotation
        );

        var points = curve.getPoints( 50 );
        var geometry = new THREE.BufferGeometry().setFromPoints( points );
        //geometry.rotateX(Math.PI / 2);
        var material = new THREE.LineBasicMaterial( { color : ringColor, transparent: true } );
        material.opacity = opacity;

        // Create the final object to add to the scene
        var ellipse = new THREE.Line( geometry, material );
        scene.add(ellipse);
}

function addLabel( name, location, color, opacity, size, font ) {
        var textGeo = new THREE.TextBufferGeometry( name, {
            font: font,
            size: size,
            height: 1,
            curveSegments: 1
        });
        var textMaterial = new THREE.MeshBasicMaterial( { color: color } );
        textMaterial.opacity = opacity;
        var textMesh = new THREE.Mesh( textGeo, textMaterial );
        textMesh.position.copy( location );
        textMesh.lookAt(camera.position);
        scene.add( textMesh );
}			

// function setControls() {
//     var controls = new THREE.TrackballControls( camera );
//     controls.rotateSpeed = 1.0;
//     controls.zoomSpeed = 1.2;
//     controls.panSpeed = 0.8;
//     controls.noZoom = false;
//     controls.noPan = false;
//     controls.staticMoving = true;
//     controls.dynamicDampingFactor = 0.3;
//     controls.keys = [ 65, 83, 68 ];
//     //controls.addEventListener( 'change', animate );
//     document.addEventListener( 'mousemove', onDocumentMouseMove, false );
//     document.addEventListener( 'mousedown', onDocumentMouseDown, false );
//     document.addEventListener( 'mouseup', onDocumentMouseUp, false );
//     document.addEventListener( 'mousewheel', onDocumentMouseWheel, false );

// }

function onWindowResize() {

    for (var i = 0; i < sceneInfos.length; i++)
    {
        sceneIn = sceneInfos[i];
        var viewerDiv = sceneIn.renderer.domElement.parentElement.parentElement;
        var width = viewerDiv.clientWidth;
        var height = viewerDiv.clientHeight;
        sceneIn.renderer.setSize(width, height);
        sceneIn.cssRenderer.setSize(width, height);
        //console.console.log("width: " + width + " height: " + height);
        //sceneIn.renderer.setPixelRatio( width / height );
        //sceneIn.cssRenderer.setPixelRatio( width / height );

        sceneIn.camera.aspect = width / height;
        sceneIn.camera.updateProjectionMatrix();
        
        //sceneInf.cssRenderer
        //var canvas = renderer.domElement;                
        //var rect = sceneIn.elem.getBoundingClientRect();
        //sceneIn.camera.aspect = rect.width / rect.height;
        //sceneIn.camera.updateProjectionMatrix();
        //sceneIn.renderer.setSize( rect.width, rect.height ); 
        //sceneIn.renderer.setPixelRatio( rect.width / rect.height );
    }    
}


function onDocumentKeyDown(event) {
    var sceneInfo;
    for (var i = 0; i < sceneInfos.length;i++)
    {
        if (event.currentTarget == sceneInfos[i].elem)
            sceneInfo = sceneInfos[i];
    }   
    if (sceneInfo)
    {
        var vector = new THREE.Vector3();
        var dir = new THREE.Vector3();
        var position = sceneInfo.camera.position;
    
        var rect = sceneInfo.elem.getBoundingClientRect();
        var left = rect.width / 2.0;
        var top = rect.height / 2.0;
    



        sceneInfo.camera.updateMatrixWorld( true );
        vector.set((left/rect.width)*2 - 1, -(top/rect.height)*2 + 1, 0.5);
        vector.unproject(sceneInfo.camera);
        
        var dir = vector.sub(position).normalize();
                            
        if (event.code == 'KeyW' || event.code == 'KeyS')					
        {
            
            if (event.code == 'KeyW') {							
                sceneInfo.futureTarget = sceneInfo.camera.position.clone().add(dir.multiplyScalar(3));
            }
            else if (event.code == 'KeyS')
            {
                sceneInfo.futureTarget = sceneInfo.camera.position.clone().add(dir.multiplyScalar(-3));
            }
        }

        sceneInfo.originalRotation = new THREE.Vector3(sceneInfo.camera.rotation.x, sceneInfo.camera.rotation.y, 
            sceneInfo.camera.rotation.z)
        sceneInfo.targetRotation = new THREE.Vector3(sceneInfo.camera.rotation.x, sceneInfo.camera.rotation.y, 
            sceneInfo.camera.rotation.z)
        
        if (event.code == "ArrowLeft")
        {
            sceneInfo.targetRotation.z = sceneInfo.originalRotation.z + 0.1;
        }
        if (event.code == "ArrowRight")
        {
            sceneInfo.targetRotation.z = sceneInfo.originalRotation.z - 0.1;
        }
        if (event.code == "ArrowUp")
        {
            sceneInfo.targetRotation.x = sceneInfo.originalRotation.x + 0.1;
        }
        if (event.code == "ArrowDown")
        {
            sceneInfo.targetRotation.x = sceneInfo.originalRotation.x - 0.1;
        }
        if (event.code == "KeyR")
        {
            sceneInfo.futureTarget = new THREE.Vector3( -0.8,  -1.2,  2);
            sceneInfo.targetRotation.set(+1 * Math.PI /2, 0  , -1 * Math.PI /2, "ZYX");
        }
        if (event.code == "KeyT")
        {
            sceneInfo.futureTarget = new THREE.Vector3(30.0, 0.0, 220.0);
            sceneInfo.targetRotation.set(0, 0  , -1 * Math.PI /2, "ZYX");
        }
        if (event.code == "KeyY")
        {
            sceneInfo.futureTarget = new THREE.Vector3(30.0, 40.0, 0.0);
            sceneInfo.targetRotation.set(1 * Math.PI /2, 0, -1 * Math.PI, "ZYX");
        }
        if (event.code == "KeyU")
        {
            sceneInfo.futureTarget = new THREE.Vector3(30.0, -40.0, 0.0);
            sceneInfo.targetRotation.set(-1 * Math.PI /2, 0, 1 * Math.PI, "ZYX");	
        }
    }
    // camera.position.x += dir.x * event.wheelDeltaY / 90.0;
    // camera.position.y += dir.y * event.wheelDeltaY / 90.0;
    // camera.position.z += dir.z * event.wheelDeltaY / 90.0;


}

function onDocumentMouseWheel( event ) {
    // camera.lookAt(futureTarget);
    
    var sceneInfo;
    for (var i = 0; i < sceneInfos.length;i++)
    {
        if (event.currentTarget == sceneInfos[i].elem)
            sceneInfo = sceneInfos[i];
    }   
    if (sceneInfo)
    {
        
        var vector = new THREE.Vector3();
        var dir = new THREE.Vector3();
        var position = sceneInfo.camera.position;

        var rect = sceneInfo.elem.getBoundingClientRect();
        var left = event.clientX - rect.left;
        var top = event.clientY - rect.top;

        sceneInfo.camera.updateMatrixWorld( true );
        vector.set((left/rect.width)*2 - 1, -(top/rect.height)*2 + 1, 0.5);
        vector.unproject(sceneInfo.camera);
        
        var dir = vector.sub(position).normalize();
        var step = 0;
        if (event.wheelDeltaY)
            step = event.wheelDeltaY / 60.0;
        else 
            step = event.deltaY * -1;
        //console.log(step);
        sceneInfo.futureTarget = sceneInfo.camera.position.clone().add(dir.multiplyScalar(step * 250));
        render();
    }
}

function onDocumentMouseDown( event ) {
    event.preventDefault();

    isMouseDown = true;
    onMouseDownPosition.x = event.clientX;
    onMouseDownPosition.y = event.clientY;

    var sceneInfo;
    for (var i = 0; i < sceneInfos.length;i++)
    {
        if (event.currentTarget == sceneInfos[i].elem)
            sceneInfo = sceneInfos[i];
    }  

    if (sceneInfo) {
        sceneInfo.originalRotation = new THREE.Vector3(sceneInfo.camera.rotation.x, sceneInfo.camera.rotation.y, 
            sceneInfo.camera.rotation.z)
        sceneInfo.targetRotation = new THREE.Vector3(sceneInfo.camera.rotation.x, sceneInfo.camera.rotation.y, 
            sceneInfo.camera.rotation.z)
        sceneInfo.originalTranslation = new THREE.Vector3(sceneInfo.camera.position.x, sceneInfo.camera.position.y, 
            sceneInfo.camera.rotation.z)
        sceneInfo.targetTranslation = new THREE.Vector3(sceneInfo.camera.position.x, sceneInfo.camera.position.y, 
            sceneInfo.camera.rotation.z)
    }

    
}

function onDocumentMouseMove( event ) {	
    event.preventDefault();	
    
    
    if ( isMouseDown ) {
        
        var isRight;
        event = event || window.event;

        var sceneInfo;
        for (var i = 0; i < sceneInfos.length;i++)
        {
            if (event.currentTarget == sceneInfos[i].elem)
                sceneInfo = sceneInfos[i];
        }  

        if ("which" in event)  // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
            isRight = event.which == 3; 
        else if ("button" in event)  // IE, Opera 
            isRight = event.button == 2; 

        if (!isRight)
        {            
            if (sceneInfo) {
                deltaRotation.x = event.clientX - onMouseDownPosition.x;
                deltaRotation.y = event.clientY - onMouseDownPosition.y;	
                sceneInfo.targetRotation.z = sceneInfo.originalRotation.z + deltaRotation.x * 0.005;							
                sceneInfo.targetRotation.x = sceneInfo.originalRotation.x + deltaRotation.y * 0.005;
            }
            render();
        }
        else 
        {
            // deltaTranslation.x = event.clientX - onMouseDownPosition.x;
            // deltaTranslation.y = event.clientY - onMouseDownPosition.y;	

            // var depth = Math.max(1, camera.position.z);      
            // var worldPoint1 = new THREE.Vector3(0, 0, depth);
            // var worldPoint2 = new THREE.Vector3(1, 0, depth);
            
            // camera.updateMatrixWorld();
            // worldPoint1.project(camera);
            // worldPoint2.project(camera);
            // console.log("worldPoint1X: " + worldPoint1.x);
            // console.log("worldPoint1Y: " + worldPoint1.y);
            // console.log("worldPoint1Z: " + worldPoint1.z);
            // console.log("worldPoint2X: " + worldPoint2.x);				
            // console.log("worldPoint2Y: " + worldPoint2.y);
            // console.log("worldPoint2Z: " + worldPoint2.z);
            // var ratio = 1 / (worldPoint2.x - worldPoint1.x);								  
            // console.log("ratio: " + ratio)

            // var factorByViewAngle = 1 + (0.5 - Math.abs(camera.rotation.y / Math.PI)) * 4;
            // ratio *= factorByViewAngle;
            // //var offset = Vector4.Transform(new Vector4(change.X * ratio, -change.Y * ratio, 0, 0), Matrix.Invert(View));
            // var offset = new THREE.Vector4(0,  -1 * deltaTranslation.x * 0.01 * ratio,  deltaTranslation.y * 0.01 * ratio, 0).applyMatrix4(camera.projectionMatrix);          
            // // Move future position            
            // futureTarget = originalTranslation.clone().add(offset);
        }
    }				
}

function onDocumentMouseUp( event ) {
    event.preventDefault();
    
    var sceneInfo;
    for (var i = 0; i < sceneInfos.length;i++)
    {
        if (event.currentTarget == sceneInfos[i].elem)
            sceneInfo = sceneInfos[i];
    }  
    isMouseDown = false;
    if (sceneInfo)
    {
        sceneInfo.targetRotation = null;
    }
    onMouseDownPosition.x = event.clientX - onMouseDownPosition.x;
    onMouseDownPosition.y = event.clientY - onMouseDownPosition.y;
    if ( onMouseDownPosition.length() > 5 ) {
        return;
    }
}


function animate() {
    //requestAnimationFrame( animate );
    render();
}

function render() {
    for (var i = 0; i < sceneInfos.length; i++) {
        if (i >= numViews)
            continue;

        sceneInf = sceneInfos[i];
        sceneInf.renderer.render(sceneInf.scene, sceneInf.camera );
        sceneInf.cssRenderer.render(sceneInf.cssScene,  sceneInf.camera);
        //sceneInf.controls.update();
        sceneInf.stats.update();
    }
}

function init_color_map() {
    var table = [];
    table.push({ r: 255, g: 0, b: 0 });
    table.push({ r: 255, g: 255, b: 0 });
    table.push({ r: 0, g: 255, b: 0 });
    var ret = [];
    for (var i = 0; i < 256; i++)
    {
        var c1_index = Math.floor(2 * i / 256);
        var c2_index = c1_index + 1;
        var c1_factor = c2_index - 2 * i / 256;
        var c2_factor = 1 - c1_factor;
        var r = Math.floor(table[c1_index].r * c1_factor + table[c2_index].r * c2_factor);
        var g = Math.floor(table[c1_index].g * c1_factor + table[c2_index].g * c2_factor);
        var b = Math.floor(table[c1_index].b * c1_factor + table[c2_index].b * c2_factor);
        ret.push({ "r" : r, "g": g, "b" : b});
    }
    return ret;
}   
