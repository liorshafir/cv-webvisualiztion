function addImage(width, height, arr)
{
    // To decode an rgb array
    //     var c = document.getElementById("frameCanvas");
    //     var ctx = c.getContext("2d");
    //     var r,g,b;

    //    for(var i=0; i< height; i++){
    //        for(var j=0; j< width; j++){
    //            var ind = 3 * (i * width + j);
    //            r = arr[ind];
    //            g = arr[ind+1];
    //            b = arr[ind+2];
    //            ctx.fillStyle = "rgba("+r+","+g+","+b+", 1)"; 
    //            ctx.fillRect( j, i, 1, 1 );
    //        }
    //    }

    // To decode a jpeg image 
    var blob = new Blob( [arr], { type: "image/jpeg" } );
    var imageUrl = URL.createObjectURL( blob );
    var img = document.querySelector( "#frameImage" );
    img.src = imageUrl;
}