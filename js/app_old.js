



loader.load( '../three.js-master/examples/fonts/gentilis_regular.typeface.json', function ( font ) {
    
    socket = new WebSocket('ws://127.0.0.1:8080');
    socket.binaryType = 'arraybuffer';
    socket.addEventListener('open', function (event) {
        
    });

    socket.addEventListener('message', function (event) {
        var bytes = new Uint8Array(event.data);/* the data you just read, in an object of type "Uint8Array" */
        var buf = new flatbuffers.ByteBuffer(bytes);
        // Get an accessor to the root object inside the buffer.
        var newFrame = Cv.WebViewer.Frame.getRootAsFrame(buf);

        geometry = new THREE.BufferGeometry();
        var vertices = [];
        var numPoints = newFrame.pointsLength();
        
        sprite = new THREE.TextureLoader().load( "../three.js-master/examples/textures/sprites/ball.png" );
        var colors = new Float32Array(numPoints * 3 );
        var i = 0;
        
        var color = new THREE.Color();
        for ( var ind = 0; ind < numPoints; ind++ ) {
            var point = newFrame.points(ind);
            if (point.x() != 0 || point.y() != 0 || point.z() != 0)
            {
                var vertex = new THREE.Vector3();
                vertex.x = point.x();
                vertex.y = point.y();
                vertex.z = point.z();
                
                vertices.push(vertex.x, vertex.y, vertex.z);
                var reflectivity = point.r();
                color = new THREE.Color().setRGB(colormap[reflectivity].r, colormap[reflectivity].g, colormap[reflectivity].b);	
                color.toArray( colors, i * 3 );
                i++;
            }
        }
        geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
        geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) );
        material = new THREE.PointsMaterial( { size: 4.25, map: sprite, sizeAttenuation: false, vertexColors: THREE.VertexColors} );
        material.color.setHSL( 0.8, 0.7, 0.1 );
        if (typeof particles !== 'undefined') {
            particles.geometry = geometry;
        } else
        {
            particles = new THREE.Points( geometry, material );
            scene.add( particles );
        }
        render();
    });	

    init();
    initWorld(font);
    animate();
                                
});


function init() {
    container = document.createElement( 'div' );
    $('#container')[0].appendChild(container);
    //document.body.appendChild( container );
    camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 0.4, 400 );
    camera.position.set( -0.8,  -1.2,  5 );
    futureTarget = new THREE.Vector3( -0.8,  -1.2,  2);
    camera.rotation.set(+1 * Math.PI /2, 0  , -1 * Math.PI /2, "ZYX")
    //camera.lookAt(currentTarget);
    
    //controls = new THREE.OrbitControls( camera );
    
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    //renderer.setPixelRatio( window.devicePixelRatio );
    
    container.appendChild( renderer.domElement );
    
    stats = new Stats();
    container.appendChild( stats.dom );
    renderer.gammaInput = true;
    renderer.gammaOutput = true;



    var imgTexture = null;
    var shininess = 50, specular = 0x333333;
    var materials = [];
    var cubeWidth = 1000;
    var numberOfSphersPerSide = 5;
    var sphereRadius = 0.02;
    //var stepSize = 1.0 / numberOfSphersPerSide;
    //geometry = new THREE.SphereBufferGeometry( sphereRadius, 32, 32 );
    
    //geometry = new THREE.Geometry();
    //Alternative
    //#geometry = new THREE.BufferGeometry();
    //#var vertices = [];
    
    
    //#sprite = new THREE.TextureLoader().load( "../three.js-master/examples/textures/sprites/ball.png" );
    //#var colors = new Float32Array( frameData.points.length * 3 );
    //#var i = 0;
    
    //#var color = new THREE.Color();
    //#for ( var ind = 0; ind < frameData.points.length; ind++ ) {
        //#var point = frameData.points[ind];
        //#if (point.x != 0 || point.y != 0 || point.z != 0)
        //#{
            //#var vertex = new THREE.Vector3();
            //vertex.x = point.z;
            //vertex.y = point.x;
            //vertex.z = point.y;
            //#vertex.x = point.x;
            //#vertex.y = point.y;
            //#vertex.z = point.z;
            

            //#vertices.push(vertex.x, vertex.y, vertex.z);

            //#color = new THREE.Color().setRGB(colormap[point.r].r, colormap[point.r].g, colormap[point.r].b);	
            //#color.toArray( colors, i * 3 );
            //#i++;
            // var diffuseColor = new THREE.Color().setHSL( (ind /frameData.points.length), 0.5, (ind /frameData.points.length)  * 0.5 + 0.1 );
            // var material = new THREE.MeshBasicMaterial( {
            // 	map: imgTexture,
            // 	color: diffuseColor,
            // 	reflectivity: (ind /frameData.points.length),
            // 	envMap: null
            // } );
            // var mesh = new THREE.Mesh( geometry, material );
            // mesh.position.x = point.x;
            // mesh.position.y = point.y;
            // mesh.position.z = point.z;
            // scene.add( mesh );
        //#}
    //#}

    //Alternative
    //#geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    //#geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) );
    //Alternative
    //#material = new THREE.PointsMaterial( { size: 4.25, map: sprite, sizeAttenuation: false, vertexColors: THREE.VertexColors} );
    //material = new THREE.PointsMaterial( { size: 4.25, sprite: sprite, transparent: true, sizeAttenuation: false} );
    //#material.color.setHSL( 0.8, 0.7, 0.1 );
    //#particles = new THREE.Points( geometry, material );
    //#scene.add( particles );				
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    document.addEventListener( 'mousedown', onDocumentMouseDown, false );
    document.addEventListener( 'mouseup', onDocumentMouseUp, false );
    document.addEventListener( 'wheel', onDocumentMouseWheel);
    document.addEventListener( 'keydown', onDocumentKeyDown);

    $( "#addframe" ).click(function() {
        addFrame();
    });

     $('input[type=file]')[0].addEventListener('change', function() {
         var file = event.currentTarget.files[0];

         if (file.name.match(/\.(csv)$/)) {
             var reader = new FileReader();

             reader.onload = function() {
                 
                scene.remove( particles );

                var frameData = { points: [] };
                var lines = reader.result.split('\n');
                for (var i = 0; i < lines.length; i++)
                {
                    if (lines[i].length > 1)
                    {
                        lineValues = lines[i].split(',');
                        frameData.points.push({
                            x: parseFloat(lineValues[0]),
                            y: parseFloat(lineValues[1]),
                            z: parseFloat(lineValues[2]),
                            r: Math.floor(parseFloat(lineValues[3]))							
                        })
                    }
                }
                geometry = new THREE.Geometry();
                sprite = new THREE.TextureLoader().load( "../three.js-master/examples/textures/sprites/ball.png" );
                colors = []
                var i = 0;
                for ( var ind = 0; ind < frameData.points.length; ind++ ) {
                    var point = frameData.points[ind];
                    if (point.x != 0 || point.y != 0 || point.z != 0)
                    {
                        var vertex = new THREE.Vector3();
                        vertex.x = point.x / 100;
                        vertex.y = point.y / 100;
                        vertex.z = point.z / 100;
                        geometry.vertices.push( vertex );

                        colors[ i ] = new THREE.Color().setRGB(colormap[point.r].r, colormap[point.r].g, colormap[point.r].b);	
                        i++;

                    }
                }								

                geometry.colors = colors;
                material = new THREE.PointsMaterial( { size: 4.25, map: sprite, vertexColors: THREE.VertexColors, alphaTest: 0.5, transparent: true, sizeAttenuation: false} );
                material.color.setHSL( 0.8, 0.7, 0.1 );
                particles = new THREE.Points( geometry, material );
                scene.add( particles );	







             };

             reader.readAsText(file);    
         } else {
             alert("File not supported, .csv files only");
         }
     });
    // document.addEventListener('DOMMouseScroll', onDocumentMouseWheel);
    window.addEventListener( 'resize', onWindowResize, false );
}

function addFrame() {
    $('input[type=file]').trigger('click');
}

function initWorld(font) {
    
    for (var rad = 0; rad <= 150; rad++) {
        drawRing(rad, 0x7595ff, 0.235);
    }

    for (var rad = 0; rad <= 150; rad+=10) {
        //addLabel( rad.toString(), new THREE.Vector3( rad, 0, 0 ), 0x7595ff, 0.47 ,0.8, font);
        drawRing(rad, 0x7595ff, 0.47);
    }

    for (var rad = 0; rad <= 150; rad+=50) {
        drawRing(rad, 0x7595ff, 0.9);
    }
    
}

function init_color_map() {
    var table = [];
    table.push({ r: 255, g: 0, b: 0 });
    table.push({ r: 255, g: 255, b: 0 });
    table.push({ r: 0, g: 255, b: 0 });

    var ret = [];

    for (var i = 0; i < 256; i++)
    {
        var c1_index = Math.floor(2 * i / 256);
        var c2_index = c1_index + 1;
        var c1_factor = c2_index - 2 * i / 256;
        var c2_factor = 1 - c1_factor;

        var r = Math.floor(table[c1_index].r * c1_factor + table[c2_index].r * c2_factor);
        var g = Math.floor(table[c1_index].g * c1_factor + table[c2_index].g * c2_factor);
        var b = Math.floor(table[c1_index].b * c1_factor + table[c2_index].b * c2_factor);


        ret.push({ "r" : r, "g": g, "b" : b});
    }

    return ret;

}

function drawRing(rad, ringColor, opacity) {
    var curve = new THREE.EllipseCurve(
            0,  0,            // ax, aY
            rad, rad,           // xRadius, yRadius
            0,  2 * Math.PI,  // aStartAngle, aEndAngle
            false,            // aClockwise
            30                 // aRotation
        );

        var points = curve.getPoints( 50 );
        var geometry = new THREE.BufferGeometry().setFromPoints( points );
        //geometry.rotateX(Math.PI / 2);
        var material = new THREE.LineBasicMaterial( { color : ringColor, transparent: true } );
        material.opacity = opacity;

        // Create the final object to add to the scene
        var ellipse = new THREE.Line( geometry, material );
        scene.add(ellipse);
}

function addLabel( name, location, color, opacity, size, font ) {
        var textGeo = new THREE.TextBufferGeometry( name, {
            font: font,
            size: size,
            height: 1,
            curveSegments: 1
        });
        var textMaterial = new THREE.MeshBasicMaterial( { color: color } );
        textMaterial.opacity = opacity;
        var textMesh = new THREE.Mesh( textGeo, textMaterial );
        textMesh.position.copy( location );
        textMesh.lookAt(camera.position);
        scene.add( textMesh );
}			

function setControls() {
    var controls = new THREE.TrackballControls( camera );
    controls.rotateSpeed = 1.0;
    controls.zoomSpeed = 1.2;
    controls.panSpeed = 0.8;
    controls.noZoom = false;
    controls.noPan = false;
    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;
    controls.keys = [ 65, 83, 68 ];
    //controls.addEventListener( 'change', animate );
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    document.addEventListener( 'mousedown', onDocumentMouseDown, false );
    document.addEventListener( 'mouseup', onDocumentMouseUp, false );
    document.addEventListener( 'mousewheel', onDocumentMouseWheel, false );

}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}


function onDocumentKeyDown(event) {
    var vector = new THREE.Vector3();
    var dir = new THREE.Vector3();
    var position = camera.position;

    var rect = container.getBoundingClientRect();
    var left = rect.width / 2.0;
    var top = rect.height / 2.0;

    camera.updateMatrixWorld( true );
    vector.set((left/rect.width)*2 - 1, -(top/rect.height)*2 + 1, 0.5);
    vector.unproject(camera);
    
    var dir = vector.sub(position).normalize();
                        
    if (event.code == 'KeyW' || event.code == 'KeyS')					
    {
        
        if (event.code == 'KeyW') {							
            futureTarget = camera.position.clone().add(dir.multiplyScalar(3));
        }
        else if (event.code == 'KeyS')
        {
            futureTarget = camera.position.clone().add(dir.multiplyScalar(-3));
        }
    }

    originalRotation = new THREE.Vector3(camera.rotation.x, camera.rotation.y, 
    camera.rotation.z)
    targetRotation = new THREE.Vector3(camera.rotation.x, camera.rotation.y, 
    camera.rotation.z)
    
    if (event.code == "ArrowLeft")
    {
        targetRotation.z = originalRotation.z + 0.1;
    }
    if (event.code == "ArrowRight")
    {
        targetRotation.z = originalRotation.z - 0.1;
    }
    if (event.code == "ArrowUp")
    {
        targetRotation.x = originalRotation.x + 0.1;
    }
    if (event.code == "ArrowDown")
    {
        targetRotation.x = originalRotation.x - 0.1;
    }
    if (event.code == "KeyR")
    {
        futureTarget = new THREE.Vector3( -0.8,  -1.2,  2);
        targetRotation.set(+1 * Math.PI /2, 0  , -1 * Math.PI /2, "ZYX");
    }
    if (event.code == "KeyT")
    {
        futureTarget = new THREE.Vector3(30.0, 0.0, 220.0);
        targetRotation.set(0, 0  , -1 * Math.PI /2, "ZYX");
    }
    if (event.code == "KeyY")
    {
        futureTarget = new THREE.Vector3(30.0, 40.0, 0.0);
        targetRotation.set(1 * Math.PI /2, 0, -1 * Math.PI, "ZYX");
    }
    if (event.code == "KeyU")
    {
        futureTarget = new THREE.Vector3(30.0, -40.0, 0.0);
        targetRotation.set(-1 * Math.PI /2, 0, 1 * Math.PI, "ZYX");	
    }
    // camera.position.x += dir.x * event.wheelDeltaY / 90.0;
    // camera.position.y += dir.y * event.wheelDeltaY / 90.0;
    // camera.position.z += dir.z * event.wheelDeltaY / 90.0;

    render();
}

function onDocumentMouseWheel( event ) {
    // radious -= event.wheelDeltaY;
    // var scale = ((radious / 180.0) + 1) / 3.0;
    // var nearScreenPoint = new THREE.Vector3(event.x, event.y,  0);
    // var farScreenPoint = new THREE.Vector3(event.x, event.y,  1);
    // var nearWorldPoint = nearScreenPoint.unproject(camera);
    // var farWorldPoint = farScreenPoint.unproject(camera);;
    // var direction = new THREE.Vector3(farWorldPoint.x - nearWorldPoint.x, farWorldPoint.y - nearWorldPoint.y, farWorldPoint.z - nearWorldPoint.z).normalize ();
    // scale = -(scale - 1) * 0.5;
    // futureTarget = futureTarget.add(direction.multiplyScalar(-1.66666));
    
    // camera.lookAt(futureTarget);
    var vector = new THREE.Vector3();
    var dir = new THREE.Vector3();
    var position = camera.position;

    var rect = container.getBoundingClientRect();
    var left = event.clientX - rect.left;
    var top = event.clientY - rect.top;

    camera.updateMatrixWorld( true );
    vector.set((left/rect.width)*2 - 1, -(top/rect.height)*2 + 1, 0.5);
    vector.unproject(camera);
    
    var dir = vector.sub(position).normalize();
    var step = 0;
    if (event.wheelDeltaY)
        step = event.wheelDeltaY / 60.0;
    else 
        step = event.deltaY * -1;

    futureTarget = camera.position.clone().add(dir.multiplyScalar(step));
    // camera.position.x += dir.x * event.wheelDeltaY / 90.0;
    // camera.position.y += dir.y * event.wheelDeltaY / 90.0;
    // camera.position.z += dir.z * event.wheelDeltaY / 90.0;

    render();

}

function onDocumentMouseDown( event ) {
    event.preventDefault();

    isMouseDown = true;
    onMouseDownPosition.x = event.clientX;
    onMouseDownPosition.y = event.clientY;
    originalRotation = new THREE.Vector3(camera.rotation.x, camera.rotation.y, 
    camera.rotation.z)
    targetRotation = new THREE.Vector3(camera.rotation.x, camera.rotation.y, 
    camera.rotation.z)
    console.log("Here");
    originalTranslation = new THREE.Vector3(camera.position.x, camera.position.y, 
    camera.rotation.z)
    targetTranslation = new THREE.Vector3(camera.position.x, camera.position.y, 
    camera.rotation.z)
}

function onDocumentMouseMove( event ) {	
    event.preventDefault();	
    if ( isMouseDown ) {
        
        var isRight;
        event = event || window.event;

        if ("which" in event)  // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
            isRight = event.which == 3; 
        else if ("button" in event)  // IE, Opera 
            isRight = event.button == 2; 

        if (!isRight)
        {	
            deltaRotation.x = event.clientX - onMouseDownPosition.x;
            deltaRotation.y = event.clientY - onMouseDownPosition.y;	
            // if (Math.abs(deltaRotation.x) > 5)
            // {	
                targetRotation.z = originalRotation.z + deltaRotation.x * 0.005;							
            // }
            // if (Math.abs(deltaRotation.y) > 5)
            // {
                targetRotation.x = originalRotation.x + deltaRotation.y * 0.005;
            //}						
        }
        else 
        {
            deltaTranslation.x = event.clientX - onMouseDownPosition.x;
            deltaTranslation.y = event.clientY - onMouseDownPosition.y;	

            var depth = Math.max(1, camera.position.z);      
            var worldPoint1 = new THREE.Vector3(0, 0, depth);
            var worldPoint2 = new THREE.Vector3(1, 0, depth);
            
            camera.updateMatrixWorld();
            worldPoint1.project(camera);
            worldPoint2.project(camera);
            console.log("worldPoint1X: " + worldPoint1.x);
            console.log("worldPoint1Y: " + worldPoint1.y);
            console.log("worldPoint1Z: " + worldPoint1.z);
            console.log("worldPoint2X: " + worldPoint2.x);				
            console.log("worldPoint2Y: " + worldPoint2.y);
            console.log("worldPoint2Z: " + worldPoint2.z);
            var ratio = 1 / (worldPoint2.x - worldPoint1.x);								  
            console.log("ratio: " + ratio)

            var factorByViewAngle = 1 + (0.5 - Math.abs(camera.rotation.y / Math.PI)) * 4;
            ratio *= factorByViewAngle;
            //var offset = Vector4.Transform(new Vector4(change.X * ratio, -change.Y * ratio, 0, 0), Matrix.Invert(View));
            var offset = new THREE.Vector4(0,  -1 * deltaTranslation.x * 0.01 * ratio,  deltaTranslation.y * 0.01 * ratio, 0).applyMatrix4(camera.projectionMatrix);
            
            // Move future position
            
            futureTarget = originalTranslation.clone().add(offset);
        }
    }				
    render();
}

function onDocumentMouseUp( event ) {

    event.preventDefault();

    isMouseDown = false;
    targetRotation = null;
    onMouseDownPosition.x = event.clientX - onMouseDownPosition.x;
    onMouseDownPosition.y = event.clientY - onMouseDownPosition.y;

    if ( onMouseDownPosition.length() > 5 ) {

        return;

    }

    render();

}


function animate() {
    //requestAnimationFrame( animate );
    render();
    stats.update();
}

function render() {
    //var timer = Date.now() * 0.00025;
    //camera.position.x = Math.cos( timer ) * 800;
    //camera.position.z = Math.sin( timer ) * 800;
    //camera.lookAt( scene.position );
    if (targetRotation)
    {
        
        camera.rotation.x += (targetRotation.x - camera.rotation.x) * 0.3;
            
        camera.rotation.z +=  (targetRotation.z - camera.rotation.z) * 0.3;
    }
    camera.position.add(futureTarget.clone().sub(camera.position).multiplyScalar(0.2));

    renderer.render( scene, camera );
}