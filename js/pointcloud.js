
// function generateNewPointCloud(vertices, colors) 
// {
//     geometry = new THREE.BufferGeometry();
//     sprite = new THREE.TextureLoader().load( "../three.js-master/examples/textures/sprites/ball.png" );
//     geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
//     geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) );
//     material = new THREE.PointsMaterial( { size: 4.25, map: sprite, sizeAttenuation: false, vertexColors: THREE.VertexColors} );
//     material.color.setHSL( 0.8, 0.7, 0.1 );
//     if (typeof particles !== 'undefined') {
//         particles.geometry = geometry;
//     } else
//     {
//         particles = new THREE.Points( geometry, material );
//         //scene.add( particles );
//     }  
// }

function updatePointCloud(vertices, colors) 
{
    var num_points =vertices.length / 3;
    
    if (app.current_pc)
    {
        var geometry = app.current_pc;
        var k = 0;
        var l = geometry.vertices.length;
        var v;
        var color;
        for ( var i = 0; i < num_points; i ++ ) {
            if (i >= l) {
                v = new THREE.Vector3( vertices[ i * 3 ], 
                    vertices[i * 3 + 1], vertices[i * 3 + 2] );
                geometry.vertices.push(v);
                reflectivity = colors[i];
                color = new THREE.Color().setRGB(colormap[reflectivity].r, colormap[reflectivity].g, colormap[reflectivity].b);	
                geometry.colors.push(color.clone());   
                geometry.verticesNeedUpdate = true;
                geometry.colorsNeedUpdate = true;
            } else {
                v = geometry.vertices[i];
                v.setX(vertices[i * 3] * 100);
                v.setY(vertices[i * 3 + 1] * 100);
                v.setZ(vertices[i * 3 + 2] * 100);
                reflectivity = colors[i];
                col = geometry.colors[i];
                col.setRGB(colormap[reflectivity].r, colormap[reflectivity].g, colormap[reflectivity].b);
                geometry.verticesNeedUpdate = true;
                geometry.colorsNeedUpdate = true;
            }
        }
        return 1;
    }
    else
    {
        // ToDo: Follow the latte practice and update the the vertices coordinates with existing buffers, rather than creating new buffers.
        geometry = new THREE.Geometry();
        //geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
        //geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) );       
        var color, reflectivity;
        for (var i = 0; i < num_points; i++) {
            // creates new vector from a cluster and adds to geometry
            var v = new THREE.Vector3( vertices[ 3 * i ] * 100, 
                vertices[ 3 * i + 1 ] * 100, vertices[ 3 * i + 2 ]  * 100);
    
            // add vertex to geometry
            geometry.vertices.push( v );
            reflectivity = colors[i];
            color = new THREE.Color().setRGB(colormap[reflectivity].r, colormap[reflectivity].g, colormap[reflectivity].b);	
            geometry.colors.push(color.clone());
        }
                  
        for (var i = 0; i < sceneInfos.length; i++)
        {
            var sceneInfo = sceneInfos[i]
            sprite = new THREE.TextureLoader().load( "../three.js-master/examples/textures/sprites/ball.png" );
            var material = new THREE.PointsMaterial( { size: 1.6    , map: sprite, sizeAttenuation: false, vertexColors: THREE.VertexColors} );
            //var material = new THREE.PointsMaterial( { size: 1.6, sizeAttenuation: true, vertexColors: THREE.VertexColors} );
            material.color.setHSL( 0.8, 0.7, 0.1 );
            var particles = new THREE.Points( geometry, material);
            app.current_pc = geometry;
            sceneInfo.scene.add(particles);
        }
        return 0;
    }
}