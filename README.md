# CV Web Viewer
![Alt text](/doc/snapshot1.png)

*  Download the repository
*  Run a local web server that will serve index.html (run it from the root folder of this repository)

//Python 2.x
python -m SimpleHTTPServer

//Python 3.x
python -m http.server

*  Run a local server that will listen to websocket on port 8080

e.g., run main.py from /samples/python/

*  Open your browser at http://localhost:8000